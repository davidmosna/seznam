//
// Created by David Mošna on 09/11/2020.
//

#ifndef SEZNAM_URLUTILS_H
#define SEZNAM_URLUTILS_H

#include <string>

using namespace std;

class URLUtils {

public:
    static bool validate(const string& url);
    static string extendURL(string origURL, string argName, string argValue);
};


#endif //SEZNAM_URLUTILS_H
