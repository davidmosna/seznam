//
// Created by David Mošna on 09/11/2020.
//

#include "URLUtils.h"
#include "Base64.h"
#include <regex>

using namespace std;

bool URLUtils::validate(const string& url)
{
    regex url_regex(
            "https?:\\/\\/(www\\.)?"
            "[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\."
            "[a-z]{2,4}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)");

    return regex_match(url, url_regex);
};

string URLUtils::extendURL(string origURL, string argName, string argValue)
{
    if (!URLUtils::validate(origURL))
    {
        throw "Invalid URL";
    }

    string value = base64_encode(reinterpret_cast<unsigned char*>(const_cast<char*>(argValue.c_str())), (unsigned int)argValue.size());

    regex queryRegex("^[^?#]+\\?([^#]+)");

    if (regex_match(origURL, queryRegex))
    {
        origURL.append("&" + argName + "=" + value);
    } else
    {
        origURL.append("?" + argName + "=" + value);
    }

    return origURL;
}
