
#include "URLUtils.h"
#include <iostream>

int main()
{
    try {
        cout << URLUtils::extendURL("https://www.seznam.cz/?q=hello+world","hiring","true") << endl;
    } catch (const char * msg)
    {
        cout << "Exception occured: " << msg << endl;
    }

    return 0;
}
