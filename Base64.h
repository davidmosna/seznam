//
// Created by David Mošna on 10/11/2020.
//

#ifndef SEZNAM_BASE64_H
#define SEZNAM_BASE64_H

#include <vector>
#include <string>

using namespace std;

typedef unsigned char BYTE;

std::string base64_encode(BYTE const* buf, unsigned int bufLen);
std::vector<BYTE> base64_decode(std::string const&);

#endif //SEZNAM_BASE64_H
