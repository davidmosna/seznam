# URLUtils

URLUtils is a sample c++ application that allows manipulation with URLs.

## Build

Ensure you have cmake and make installed, if so, generate makefile

```bash
cmake .
```
then build the application

```bash
make
```

and run it

```bash
./seznam
```